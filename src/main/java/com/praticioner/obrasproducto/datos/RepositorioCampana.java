package com.praticioner.obrasproducto.datos;

import com.praticioner.obrasproducto.modelo.Campana;
import com.praticioner.obrasproducto.modelo.Obrasocial;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioCampana  extends MongoRepository<Campana,String> {
}
