package com.praticioner.obrasproducto.datos;

import com.praticioner.obrasproducto.modelo.Obrasocial;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RepositorioObrasocial extends MongoRepository<Obrasocial,String> {
}
