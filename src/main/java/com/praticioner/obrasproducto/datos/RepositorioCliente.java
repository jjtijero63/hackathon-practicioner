package com.praticioner.obrasproducto.datos;

import com.praticioner.obrasproducto.modelo.Campana;
import com.praticioner.obrasproducto.modelo.Cliente;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface RepositorioCliente extends MongoRepository<Cliente,String> {


    @Query(value = "{ 'clientenumero' : ?0}")
    Cliente findPorNumero(String brand);

    @Query(value = "{ 'documentonumero' : ?0}")
    Cliente findPorDocumento(String brand);



}
