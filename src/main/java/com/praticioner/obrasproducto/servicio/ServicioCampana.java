package com.praticioner.obrasproducto.servicio;

import com.praticioner.obrasproducto.modelo.Campana;
import com.praticioner.obrasproducto.modelo.Cliente;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ServicioCampana {

    public Campana creaCampanaDeCliente (Cliente cliente , Campana campana);
    public Campana actualizaCampanaDeCliente (Cliente cliente , Campana campana);
    public Campana actualizaParteCampanaDeCliente (Cliente cliente  ,Campana campana);
    public boolean borrarCampanaDeCliente (Cliente cliente  ,int idCliente);
}
