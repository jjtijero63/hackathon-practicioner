package com.praticioner.obrasproducto.servicio;

import com.praticioner.obrasproducto.modelo.Campana;
import com.praticioner.obrasproducto.modelo.Cliente;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface ServicioCliente {

    public Cliente crearCliente(Cliente cliente);
    public List<Cliente> obtenerClientes();
//    public Optional<Cliente> obtenerCampanaPorCliente(String id);

    public Cliente crearClienteTotal(Cliente cliente);


    public Optional<Cliente> obtenerClientePorId(String id);
    public Cliente obtenerClientePorClientenumero(String id);
    public  Cliente obtenerClientePorClienteDocumento(String id);


    public Cliente actualizarCliente(Cliente cliente);
    public Cliente actualizarParteCliente(Cliente cliente,Cliente clienteActualizar);
    public  void borrarClientePorID (String id);
    public List<Campana> obtenerCampanasCliente(String clientenumero);

    public List<Campana> obtenerCampanasClientePorDocumento(String idCliente);

}
