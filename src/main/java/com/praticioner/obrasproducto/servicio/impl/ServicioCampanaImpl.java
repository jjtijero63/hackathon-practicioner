package com.praticioner.obrasproducto.servicio.impl;

import com.praticioner.obrasproducto.datos.RepositorioCampana;
import com.praticioner.obrasproducto.datos.RepositorioCliente;
import com.praticioner.obrasproducto.modelo.Campana;
import com.praticioner.obrasproducto.modelo.Cliente;
import com.praticioner.obrasproducto.servicio.ServicioCampana;
import com.praticioner.obrasproducto.servicio.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class ServicioCampanaImpl  implements ServicioCampana {



    @Autowired
    private RepositorioCliente repositorioCliente;



    @Autowired
    ServicioCliente serviciocliente;

    //private final AtomicLong secuencuiaIds= new AtomicLong(0L);
    private final AtomicInteger secuencuiaIds = new AtomicInteger(0);


    @Override
    public Campana creaCampanaDeCliente (Cliente cliente , Campana campana)
    {
        System.out.println("this.secuencuiaIds.incrementAndGet() ::" + this.secuencuiaIds.incrementAndGet());
        //campana.setIdcam(this.secuencuiaIds.incrementAndGet());
        cliente.getCampanas().add(campana);
        this.repositorioCliente.save(cliente);
        return  campana;
    }



    @Override
    public Campana actualizaCampanaDeCliente (Cliente cliente , Campana campana)
    {
        final List<Campana> usuarios = cliente.getCampanas();
        if  (usuarios != null)
        {

            for (int i=0; i< cliente.getCampanas().size();i++)
            {
                if(cliente.getCampanas().get(i).getIdcam() == campana.getIdcam())
                {
                    cliente.getCampanas().set(i,campana);
                    this.repositorioCliente.save(cliente);
                    return  campana;
                }
            }



        }
        return null;
    }


    @Override
    public Campana actualizaParteCampanaDeCliente (Cliente p , Campana campana)
    {
        final List<Campana> campanas = p.getCampanas();
        if  (campanas != null)
        {

            for (int i=0; i< p.getCampanas().size();i++)
            {
                if(p.getCampanas().get(i).getIdcam() == campana.getIdcam())
                {
                    if (campana.getCampanatipoproducto() != null && campana.getCampanatipoproducto().trim().length() > 0) {
                        p.getCampanas().get(i).setCampanatipoproducto(campana.getCampanatipoproducto());
                    }

                    if (campana.getCampanacodigo() != null && campana.getCampanacodigo().trim().length() > 0) {
                        p.getCampanas().get(i).setCampanacodigo(campana.getCampanacodigo());
                    }
                    if (campana.getCampanadescripcion() != null && campana.getCampanadescripcion().trim().length() > 0) {
                        p.getCampanas().get(i).setCampanadescripcion(campana.getCampanadescripcion());
                    }
                    if (campana.getCampanaimporte() != null && campana.getCampanaimporte().trim().length() > 0) {
                        p.getCampanas().get(i).setCampanaimporte(campana.getCampanaimporte());
                    }
                    if (campana.getCampanamoneda() != null && campana.getCampanamoneda().trim().length() > 0) {
                        p.getCampanas().get(i).setCampanamoneda(campana.getCampanamoneda());
                    }
                    if (campana.getCampanafechavencimiento() != null && campana.getCampanafechavencimiento().trim().length() > 0) {
                        p.getCampanas().get(i).setCampanafechavencimiento(campana.getCampanafechavencimiento());
                    }
                    if (campana.getCampanatarjeta() != null && campana.getCampanatarjeta().trim().length() > 0) {
                        p.getCampanas().get(i).setCampanatarjeta(campana.getCampanatarjeta());
                    }
                    if (campana.getCodigoobra() != null && campana.getCodigoobra().trim().length() > 0) {
                        p.getCampanas().get(i).setCodigoobra(campana.getCodigoobra());
                    }
                    if (campana.getDescripcionobra() != null && campana.getDescripcionobra().trim().length() > 0) {
                        p.getCampanas().get(i).setDescripcionobra(campana.getDescripcionobra());
                    }

                    if (campana.getPorcentajeapoyo() != null && campana.getPorcentajeapoyo().trim().length() > 0) {
                        p.getCampanas().get(i).setPorcentajeapoyo(campana.getPorcentajeapoyo());
                    }


                    this.repositorioCliente.save(p);
                    return  campana;
                }
            }
        }
        return null;
    }


    @Override
    public boolean borrarCampanaDeCliente (Cliente p ,int idCampana)
    {
        final List<Campana> campanas = p.getCampanas();
        if  (campanas != null)
        {

            for (int i=0; i< p.getCampanas().size();i++)
            {
                if(p.getCampanas().get(i).getIdcam() == idCampana)
                {
                    System.out.println("borrar dddd: " + i);
                    p.getCampanas().remove(i);
                    System.out.println("borrar dddd");
                    this.repositorioCliente.save(p);
                    return  true;
                }
            }



        }
        return false;
    }

}
