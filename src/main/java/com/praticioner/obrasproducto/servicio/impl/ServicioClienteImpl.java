package com.praticioner.obrasproducto.servicio.impl;

import com.praticioner.obrasproducto.datos.RepositorioCampana;
import com.praticioner.obrasproducto.datos.RepositorioCliente;
import com.praticioner.obrasproducto.modelo.Campana;
import com.praticioner.obrasproducto.modelo.Cliente;
import com.praticioner.obrasproducto.servicio.ServicioCampana;
import com.praticioner.obrasproducto.servicio.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class ServicioClienteImpl implements ServicioCliente {

    @Autowired
    private RepositorioCliente repositorioCliente;

    @Override
    public Cliente crearCliente(Cliente cliente)
    {
        cliente.setCampanas(new ArrayList<Campana>());
        return  this.repositorioCliente.insert(cliente);
    }

    @Override
    public Cliente crearClienteTotal(Cliente cliente)
    {
        //cliente.setCampanas(new ArrayList<Campana>());
        return  this.repositorioCliente.insert(cliente);
    }



    @Override
    public List<Cliente> obtenerClientes()
    {
        return  this.repositorioCliente.findAll();
    }


    @Override
    public Optional<Cliente> obtenerClientePorId(String id)
    {

        return  this.repositorioCliente.findById(id);
    }

    @Override
    public  Cliente obtenerClientePorClientenumero(String id)
    {

        return  this.repositorioCliente.findPorNumero(id);
    }

    @Override
    public  Cliente obtenerClientePorClienteDocumento(String id)
    {

        return  this.repositorioCliente.findPorDocumento(id);
    }


    @Override
    public Cliente actualizarCliente(Cliente cliente)
    {



        Cliente p = this.obtenerClientePorClientenumero(cliente.getClientenumero());
        if(p == null )
        {
            return null;
        }
        //  Cliente p = cp.get();
        cliente.setCampanas(p.getCampanas());

        return  this.repositorioCliente.save(cliente);
    }

    @Override
    public Cliente actualizarParteCliente(Cliente cliente,Cliente clienteActualizar)
    {

        if (cliente.getClientenumero() != null && cliente.getClientenumero().trim().length() > 0) {
            clienteActualizar.setClientenumero(cliente.getClientenumero());
        }
        if (cliente.getDocumentocodigotipo() != null && cliente.getDocumentocodigotipo().trim().length() > 0) {
            clienteActualizar.setDocumentocodigotipo(cliente.getDocumentocodigotipo());
        }

        if (cliente.getDocumentodescripciontipo() != null && cliente.getDocumentodescripciontipo().trim().length() > 0) {
            clienteActualizar.setDocumentodescripciontipo(cliente.getDocumentodescripciontipo());
        }

        if (cliente.getDocumentonumero() != null && cliente.getDocumentonumero().trim().length() > 0) {
            clienteActualizar.setDocumentonumero(cliente.getDocumentonumero());
        }
        if (cliente.getTitulo() != null && cliente.getTitulo().trim().length() > 0) {
            clienteActualizar.setTitulo(cliente.getTitulo());
        }

        if (cliente.getNombre() != null && cliente.getNombre().trim().length() > 0) {
            clienteActualizar.setNombre(cliente.getNombre());
        }
        if (cliente.getApellidos() != null && cliente.getApellidos().trim().length() > 0) {
            clienteActualizar.setApellidos(cliente.getApellidos());
        }
        if (cliente.getCelular() != null && cliente.getCelular().trim().length() > 0) {
            clienteActualizar.setCelular(cliente.getCelular());
        }
        if (cliente.getCorreo() != null && cliente.getCorreo().trim().length() > 0) {
            clienteActualizar.setCorreo(cliente.getCorreo());
        }
        //this.productos.set(i, producto);
        return this.repositorioCliente.save(clienteActualizar);
    }

    @Override
    public  void borrarClientePorID(String id)
    {
        this.repositorioCliente.deleteById(id);
    }

    @Override
    public List<Campana> obtenerCampanasCliente(String idCliente)
    {
        System.out.println("entro--obtenerUsuarioProducto");
        Cliente p = this.obtenerClientePorClientenumero(idCliente);
        if(p ==null )
            throw  new IllegalArgumentException("No existe el producto");
        //Cliente p = cp.get();
        final List<Campana> usuarios = p.getCampanas();

        return usuarios==null
                ? Collections.emptyList()
                : p.getCampanas()
                ;

    }


    @Override
    public List<Campana> obtenerCampanasClientePorDocumento(String idCliente)
    {
        System.out.println("entro--obtenerUsuarioProducto");
        Cliente p = this.obtenerClientePorClienteDocumento(idCliente);
        if(p ==null )
            throw  new IllegalArgumentException("No existe el producto");
        //Cliente p = cp.get();
        final List<Campana> usuarios = p.getCampanas();

        return usuarios==null
                ? Collections.emptyList()
                : p.getCampanas()
                ;

    }



}
