package com.praticioner.obrasproducto.servicio.impl;

import com.praticioner.obrasproducto.datos.RepositorioObrasocial;
import com.praticioner.obrasproducto.modelo.Obrasocial;
import com.praticioner.obrasproducto.servicio.ServicioObrasocial;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
public class ServicioObrasocialImpl  implements ServicioObrasocial {



    public ServicioObrasocialImpl() {
        System.out.println("============== ServicioObrasocialImpl");
    }

    @Autowired
    RepositorioObrasocial obrasocialRepository;

    @Override
    public Obrasocial crearObrasocial(Obrasocial obrasocial) {
        return this.obrasocialRepository.insert(obrasocial);
    }

    @Override
    public List<Obrasocial> obtenerObrasociales() {
        return this.obrasocialRepository.findAll();
    }

    @Override
    public Obrasocial obtenerObrasocialPorId(String codigoobra) {
        final Optional<Obrasocial> p = this.obrasocialRepository.findById(codigoobra);
        return p.isPresent() ? p.get() : null;
    }

    @Override
    public Obrasocial actualizarObrasocial(String codigoobra, Obrasocial obrasocialNuevo) {
        final Optional<Obrasocial> p = this.obrasocialRepository.findById(codigoobra);
        if(p.isPresent()) {
            obrasocialNuevo.setCodigoobra(codigoobra);
            if(obrasocialNuevo.getDescripcionobra() == null)
                obrasocialNuevo.setDescripcionobra(p.get().getDescripcionobra());
            if(obrasocialNuevo.getEstado() == null)
                obrasocialNuevo.setEstado(p.get().getEstado());
            if(obrasocialNuevo.getFechavigencia() == null)
                obrasocialNuevo.setFechavigencia(p.get().getFechavigencia());
            if(obrasocialNuevo.getNumeroseleccion() == null)
                obrasocialNuevo.setNumeroseleccion(p.get().getNumeroseleccion());
            if(obrasocialNuevo.getDepartamento() == null)
                obrasocialNuevo.setDepartamento(p.get().getDepartamento());
            if(obrasocialNuevo.getProvincia() == null)
                obrasocialNuevo.setProvincia(p.get().getProvincia());
            if(obrasocialNuevo.getDistrito() == null)
                obrasocialNuevo.setDistrito(p.get().getDistrito());
            if(obrasocialNuevo.getTipobra() == null)
                obrasocialNuevo.setTipobra(p.get().getTipobra());

            if(obrasocialNuevo.getPorcentajeapoyo() == null)
                obrasocialNuevo.setPorcentajeapoyo(p.get().getPorcentajeapoyo());

            this.obrasocialRepository.save(obrasocialNuevo);
            return obrasocialNuevo;
        }
        return null;
    }

    @Override
    public void borrarObrasocialPorId(String codigoobra) {
        this.obrasocialRepository.deleteById(codigoobra);
    }
}
