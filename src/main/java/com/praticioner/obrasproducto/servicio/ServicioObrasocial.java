package com.praticioner.obrasproducto.servicio;


import com.praticioner.obrasproducto.modelo.Obrasocial;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ServicioObrasocial {

    //CRUD

    //POST /obrasocial
    public Obrasocial crearObrasocial(Obrasocial obrasocial);

    //GET /obrasocial
    public List<Obrasocial> obtenerObrasociales();

    //GET /obrasocial/{codigoobra}
    public Obrasocial obtenerObrasocialPorId(String codigoobra);

    //PATCH /obrasocial/{id} @RequestBody
    public Obrasocial actualizarObrasocial(String codigoobra, Obrasocial obrasocial);

    //DELETE /obrasocial/{codigoobra}
    public void borrarObrasocialPorId(String codigoobra);
}
