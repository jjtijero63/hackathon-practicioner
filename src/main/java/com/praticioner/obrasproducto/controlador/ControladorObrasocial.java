package com.praticioner.obrasproducto.controlador;

import com.praticioner.obrasproducto.modelo.Obrasocial;
import com.praticioner.obrasproducto.servicio.ServicioObrasocial;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;


@RestController
@RequestMapping("/apitechu/v2/obrasocial")
public class ControladorObrasocial {


    @Autowired
    ServicioObrasocial servicioObrasocial;

    @GetMapping
    public List<Obrasocial> obtenerObrasociales() {
        return this.servicioObrasocial.obtenerObrasociales();
    }

    @GetMapping("/{codigoobra}")
    public ResponseEntity<Obrasocial> obtenerObrasocial(@PathVariable String codigoobra) {
        final Obrasocial p = this.servicioObrasocial.obtenerObrasocialPorId(codigoobra);
        if(p != null) {
            return ResponseEntity.ok(p);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Obrasocial agregarObrasocial(@RequestBody Obrasocial obrasocial) {
        return this.servicioObrasocial.crearObrasocial(obrasocial);
    }

    @DeleteMapping("/{codigoobra}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrarObrasocial(@PathVariable String codigoobra) {
        this.servicioObrasocial.borrarObrasocialPorId(codigoobra);
    }

    @PatchMapping("/{codigoobra}")
    public Obrasocial actualizarObrasocial(@PathVariable String codigoobra,
                                           @RequestBody Obrasocial obrasocialNuevo) {
        final Obrasocial p = this.servicioObrasocial.actualizarObrasocial(codigoobra, obrasocialNuevo);
        if(p == null)
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        return p;
    }
}
