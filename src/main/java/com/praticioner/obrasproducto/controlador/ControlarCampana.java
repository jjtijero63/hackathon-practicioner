package com.praticioner.obrasproducto.controlador;

import com.praticioner.obrasproducto.modelo.Campana;
import com.praticioner.obrasproducto.modelo.Cliente;
import com.praticioner.obrasproducto.servicio.ServicioCampana;
import com.praticioner.obrasproducto.servicio.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/apitechu/v2/clientes/{idCliente}/campanas")
public class ControlarCampana {


    @Autowired
    ServicioCliente servicioCliente;

    @Autowired
    ServicioCampana servicioCampana;

    @GetMapping
    public ResponseEntity obtenerCampanaDeCliente(@PathVariable String idCliente){
        try
        {
            return ResponseEntity.ok(this.servicioCliente.obtenerCampanasCliente(idCliente));
        } catch (IllegalArgumentException x) {
            return new ResponseEntity<>("Cliente: "+idCliente+ " no encontrado", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/Documento")
    public ResponseEntity obtenerCampanaDeClientePorDocumento(@PathVariable String idCliente){
        try
        {
            return ResponseEntity.ok(this.servicioCliente.obtenerCampanasClientePorDocumento(idCliente));
        } catch (IllegalArgumentException x) {
            return new ResponseEntity<>("Cliente: "+idCliente+ " no encontrado", HttpStatus.NOT_FOUND);
        }
    }


    @PostMapping
    public ResponseEntity creaCampanaDeCliente(@PathVariable String idCliente, @RequestBody Campana campana)
    {
        Cliente p = this.servicioCliente.obtenerClientePorClientenumero(idCliente);


        final Campana u;
        if (p == null )
        {
            return new ResponseEntity<>("Cliente: " + idCliente+ " no encontrado.", HttpStatus.NOT_FOUND);
        }
        u =  this.servicioCampana.creaCampanaDeCliente(p,campana);
        if(u!=null)
        {
            return new ResponseEntity<>("Campaña: "+ campana.getIdcam()+  " creado correctamente.", HttpStatus.OK);
        }
        return new ResponseEntity<>("Campaña : "+ campana.getIdcam() + " no creado.", HttpStatus.NOT_FOUND);
    }

    @PutMapping
    public ResponseEntity actualizaUsuarioDeProducto(@PathVariable String idCliente, @RequestBody Campana campana)
    {

        Cliente p = this.servicioCliente.obtenerClientePorClientenumero(idCliente);
       // Optional<Cliente> p = this.servicioCliente.obtenerClientePorId(idCliente);
        final Campana u;
        if (p == null)
        {
            return new ResponseEntity<>("Cliente: " + idCliente+ " no encontrado.", HttpStatus.NOT_FOUND);
        }
        u =  this.servicioCampana.actualizaCampanaDeCliente(p,campana);
        if(u!=null)
        {
            return new ResponseEntity<>("Campaña: "+ campana.getIdcam() +  " actualizado correctamente.", HttpStatus.OK);
        }
        return new ResponseEntity<>("Campaña: "+ campana.getIdcam() + " no encontrado.", HttpStatus.NOT_FOUND);
    }


    @PatchMapping("/{idCampana}")
    public ResponseEntity actualizaParteCampanaDeCliente(@PathVariable String idCliente, @PathVariable int idCampana, @RequestBody Campana campana)
    {
        Cliente p = this.servicioCliente.obtenerClientePorClientenumero(idCliente);
        //Optional<Cliente> p = this.servicioCliente.obtenerClientePorId(idUsuario);
        final Campana ca;
        campana.setIdcam(idCampana);
        if (p == null)
        {
            return new ResponseEntity<>("Cliente: " + idCliente+ " no encontrado.", HttpStatus.NOT_FOUND);
        }
        ca=  this.servicioCampana.actualizaParteCampanaDeCliente(p,campana);
        if(ca!=null)
        {
            return new ResponseEntity<>("Campaña: "+ idCampana +  " actualizado correctamente.", HttpStatus.OK);
        }
        return new ResponseEntity<>("Campaña: "+ idCampana + " no encontrado.", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{idCampana}")
    public ResponseEntity borrarUsuarioDeProducto(@PathVariable String idCliente, @PathVariable int idCampana)
    {
        Cliente p = this.servicioCliente.obtenerClientePorClientenumero(idCliente);
       // Optional<Cliente> p = this.servicioCliente.obtenerClientePorId(idCliente);
        final boolean u;
        if (p == null)
        {
            return new ResponseEntity<>("Cliente: " + idCliente+ " no encontrado.", HttpStatus.NOT_FOUND);
        }
        u =  this.servicioCampana.borrarCampanaDeCliente(p,idCampana);
        if(u ==true)
        {
            return new ResponseEntity<>("Campaña: "+ idCampana +  " borrado correctamente.", HttpStatus.OK);
        }
        return new ResponseEntity<>("Campaña: "+ idCampana + " no encontrado.", HttpStatus.NOT_FOUND);
    }

}
