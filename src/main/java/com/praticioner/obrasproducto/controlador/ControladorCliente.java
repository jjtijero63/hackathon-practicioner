package com.praticioner.obrasproducto.controlador;


import com.praticioner.obrasproducto.modelo.Campana;
import com.praticioner.obrasproducto.modelo.Cliente;
import com.praticioner.obrasproducto.servicio.ServicioCampana;
import com.praticioner.obrasproducto.servicio.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2/clientes")
public class ControladorCliente {

    @Autowired
    ServicioCliente servicioCliente;

    @GetMapping
    public List<Cliente> obtenerCliente()
    {
        return this.servicioCliente.obtenerClientes();
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente agregarCliente(@RequestBody Cliente cliente)
    {
        return this.servicioCliente.crearCliente(cliente);
    }


    @PostMapping("/total")
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente agregarClienteTotal(@RequestBody Cliente cliente)
    {
        return this.servicioCliente.crearClienteTotal(cliente);
    }


    @GetMapping("/{id}")
    public ResponseEntity obtenerCliente(@PathVariable String id)
    {
       // Cliente p = this.servicioCliente.obtenerClientePorClientenumero(idCliente);
        Cliente p = this.servicioCliente.obtenerClientePorClientenumero(id);
        if(p != null)
        {
            return ResponseEntity.ok(p);
        }
        return new ResponseEntity<>("Cliente: "+  id +  " no encontrado",HttpStatus.NOT_FOUND);
    }

    @GetMapping("/Documento/{id}")
    public ResponseEntity obtenerClienteDocumento(@PathVariable String id)
    {
        // Cliente p = this.servicioCliente.obtenerClientePorClientenumero(idCliente);
        Cliente p = this.servicioCliente.obtenerClientePorClienteDocumento(id);
        if(p != null)
        {
            return ResponseEntity.ok(p);
        }
        return new ResponseEntity<>("Cliente: "+  id +  " no encontrado",HttpStatus.NOT_FOUND);
    }


    @PutMapping
    public ResponseEntity  actualizaCliente(@RequestBody Cliente cliente)
    {
        Cliente p = this.servicioCliente.actualizarCliente(cliente);
        if (p != null)
        {
            return new ResponseEntity<>("Cliente: "+ cliente.getClientenumero() +  " actualizado correctamente.", HttpStatus.OK);
        }
        return new ResponseEntity<>("Cliente: "+ cliente.getClientenumero() + " no encontrado.", HttpStatus.NOT_FOUND);
    }


    @PatchMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    public ResponseEntity actualizaParteCliente(@RequestBody Cliente cliente)
    {
        Cliente p = this.servicioCliente.actualizarCliente(cliente);
        //Optional <Cliente> cp = this.servicioCliente.obtenerClientePorId(cliente.getClientenumero());
        if (p !=null)
        {
           // Cliente p = cp.get();
            this.servicioCliente.actualizarParteCliente(cliente,p);
            return new ResponseEntity<>("Cliente: "+ cliente.getClientenumero() +  " actualizado correctamente.", HttpStatus.OK);
        }
        return new ResponseEntity<>("Cliente: "+ cliente.getClientenumero() + " no encontrado.", HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public ResponseEntity borrarCliente(@PathVariable String id)
    {
        Cliente p = this.servicioCliente.obtenerClientePorClientenumero(id);
       // Optional <Cliente> cp = this.servicioCliente.obtenerClientePorId(id);

        if (p != null)
        {
            this.servicioCliente.borrarClientePorID(p.getId());
            return new ResponseEntity<>("Cliente: "+ id +  " borrado correctamente.", HttpStatus.OK);
        }
        return new ResponseEntity<>("Cliente: "+ id + " no encontrado.", HttpStatus.NOT_FOUND);
    }
}
