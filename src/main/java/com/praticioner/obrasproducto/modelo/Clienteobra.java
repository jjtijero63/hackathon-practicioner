package com.praticioner.obrasproducto.modelo;

public class Clienteobra {

    String campanacodigo;
    String codigoobra;
    String documentocodigotipo;
    String documentonumero;

    public Clienteobra(String campanacodigo, String codigoobra, String documentocodigotipo, String documentonumero) {
        this.campanacodigo = campanacodigo;
        this.codigoobra = codigoobra;
        this.documentocodigotipo = documentocodigotipo;
        this.documentonumero = documentonumero;
    }


    public String getCampanacodigo() {
        return campanacodigo;
    }

    public void setCampanacodigo(String campanacodigo) {
        this.campanacodigo = campanacodigo;
    }

    public String getCodigoobra() {
        return codigoobra;
    }

    public void setCodigoobra(String codigoobra) {
        this.codigoobra = codigoobra;
    }

    public String getDocumentocodigotipo() {
        return documentocodigotipo;
    }

    public void setDocumentocodigotipo(String documentocodigotipo) {
        this.documentocodigotipo = documentocodigotipo;
    }

    public String getDocumentonumero() {
        return documentonumero;
    }

    public void setDocumentonumero(String documentonumero) {
        this.documentonumero = documentonumero;
    }
}
