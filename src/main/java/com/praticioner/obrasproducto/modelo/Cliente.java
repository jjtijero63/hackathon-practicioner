package com.praticioner.obrasproducto.modelo;
// clientes y no clientes documento de identidad
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection = "cliente")
public class Cliente {


    @Id  String  id;



    String  clientenumero;
    String  documentocodigotipo;
    String  documentodescripciontipo;
    String  documentonumero;
    String  titulo;
    String  nombre;
    String  apellidos;
    String  celular;
    String  correo;

    List<Campana> campanas = new ArrayList<Campana>();



    public Cliente(String clientenumero, String documentocodigotipo, String documentodescripciontipo, String documentonumero, String titulo, String nombre, String apellidos, String celular, String correo) {
        this.clientenumero = clientenumero;
        this.documentocodigotipo = documentocodigotipo;
        this.documentodescripciontipo = documentodescripciontipo;
        this.documentonumero = documentonumero;
        this.titulo = titulo;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.celular = celular;
        this.correo = correo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getClientenumero() {
        return clientenumero;
    }

    public void setClientenumero(String clientenumero) {
        this.clientenumero = clientenumero;
    }

    public String getDocumentocodigotipo() {
        return documentocodigotipo;
    }

    public void setDocumentocodigotipo(String documentocodigotipo) {
        this.documentocodigotipo = documentocodigotipo;
    }

    public String getDocumentodescripciontipo() {
        return documentodescripciontipo;
    }

    public void setDocumentodescripciontipo(String documentodescripciontipo) {
        this.documentodescripciontipo = documentodescripciontipo;
    }

    public String getDocumentonumero() {
        return documentonumero;
    }

    public void setDocumentonumero(String documentonumero) {
        this.documentonumero = documentonumero;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public List<Campana> getCampanas() {
        return campanas;
    }

    public void setCampanas(List<Campana> campanas) {
        this.campanas = campanas;
    }

}
