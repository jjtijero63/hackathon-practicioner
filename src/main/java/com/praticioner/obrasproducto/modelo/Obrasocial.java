package com.praticioner.obrasproducto.modelo;

import org.springframework.data.annotation.Id;

public class Obrasocial {
    @Id
    String codigoobra;
    String descripcionobra;
    String estado;
    String fechavigencia;
    String numeroseleccion;
    String departamento;
    String provincia;
    String distrito;
    String tipobra;
    String porcentajeapoyo;

    public Obrasocial(String codigoobra, String descripcionobra, String estado, String fechavigencia, String numeroseleccion, String departamento, String provincia, String distrito, String tipobra, String porcentajeapoyo) {
        this.codigoobra = codigoobra;
        this.descripcionobra = descripcionobra;
        this.estado = estado;
        this.fechavigencia = fechavigencia;
        this.numeroseleccion = numeroseleccion;
        this.departamento = departamento;
        this.provincia = provincia;
        this.distrito = distrito;
        this.tipobra = tipobra;
        this.porcentajeapoyo = porcentajeapoyo;
    }

    public String getCodigoobra() {
        return codigoobra;
    }

    public void setCodigoobra(String codigoobra) {
        this.codigoobra = codigoobra;
    }

    public String getDescripcionobra() {
        return descripcionobra;
    }

    public void setDescripcionobra(String descripcionobra) {
        this.descripcionobra = descripcionobra;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getFechavigencia() {
        return fechavigencia;
    }

    public void setFechavigencia(String fechavigencia) {
        this.fechavigencia = fechavigencia;
    }

    public String getNumeroseleccion() {
        return numeroseleccion;
    }

    public void setNumeroseleccion(String numeroseleccion) {
        this.numeroseleccion = numeroseleccion;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    public String getTipobra() {
        return tipobra;
    }

    public void setTipobra(String tipobra) {
        this.tipobra = tipobra;
    }


    public String getPorcentajeapoyo() {
        return porcentajeapoyo;
    }

    public void setPorcentajeapoyo(String porcentajeapoyo) {
        this.porcentajeapoyo = porcentajeapoyo;
    }
}
