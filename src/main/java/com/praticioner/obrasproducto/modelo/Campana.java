package com.praticioner.obrasproducto.modelo;
// Modelo de datos campanas aprobadas por riesgos

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

//@Document(collection = "campana")
public class Campana {

    int idcam;
    String campanatipoproducto;
    String campanacodigo;
    String campanadescripcion;
    String campanaimporte;
    String campanamoneda;
    String campanafechavencimiento;
    String campanatarjeta;
    String codigoobra;
    String descripcionobra;
    String porcentajeapoyo;


    public Campana(int idcam, String campanatipoproducto, String campanacodigo, String campanadescripcion, String campanaimporte, String campanamoneda, String campanafechavencimiento, String campanatarjeta, String codigoobra, String descripcionobra, String porcentajeapoyo) {
        this.idcam = idcam;
        this.campanatipoproducto = campanatipoproducto;
        this.campanacodigo = campanacodigo;
        this.campanadescripcion = campanadescripcion;
        this.campanaimporte = campanaimporte;
        this.campanamoneda = campanamoneda;
        this.campanafechavencimiento = campanafechavencimiento;
        this.campanatarjeta = campanatarjeta;
        this.codigoobra = codigoobra;
        this.descripcionobra = descripcionobra;
        this.porcentajeapoyo = porcentajeapoyo;
    }

    public int getIdcam() {
        return idcam;
    }

    public void setIdcam(int idcam) {
        this.idcam = idcam;
    }

    public String getCampanatipoproducto() {
        return campanatipoproducto;
    }

    public void setCampanatipoproducto(String campanatipoproducto) {
        this.campanatipoproducto = campanatipoproducto;
    }

    public String getCampanacodigo() {
        return campanacodigo;
    }

    public void setCampanacodigo(String campanacodigo) {
        this.campanacodigo = campanacodigo;
    }

    public String getCampanadescripcion() {
        return campanadescripcion;
    }

    public void setCampanadescripcion(String campanadescripcion) {
        this.campanadescripcion = campanadescripcion;
    }

    public String getCampanaimporte() {
        return campanaimporte;
    }

    public void setCampanaimporte(String campanaimporte) {
        this.campanaimporte = campanaimporte;
    }

    public String getCampanamoneda() {
        return campanamoneda;
    }

    public void setCampanamoneda(String campanamoneda) {
        this.campanamoneda = campanamoneda;
    }

    public String getCampanafechavencimiento() {
        return campanafechavencimiento;
    }

    public void setCampanafechavencimiento(String campanafechavencimiento) {
        this.campanafechavencimiento = campanafechavencimiento;
    }

    public String getCampanatarjeta() {
        return campanatarjeta;
    }

    public void setCampanatarjeta(String campanatarjeta) {
        this.campanatarjeta = campanatarjeta;
    }

    public String getCodigoobra() {
        return codigoobra;
    }

    public void setCodigoobra(String codigoobra) {
        this.codigoobra = codigoobra;
    }

    public String getDescripcionobra() {
        return descripcionobra;
    }

    public void setDescripcionobra(String descripcionobra) {
        this.descripcionobra = descripcionobra;
    }

    public String getPorcentajeapoyo() {
        return porcentajeapoyo;
    }

    public void setPorcentajeapoyo(String porcentajeapoyo) {
        this.porcentajeapoyo = porcentajeapoyo;
    }




}
