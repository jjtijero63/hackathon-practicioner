package com.praticioner.obrasproducto.modelo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class ClienteTest {


    Cliente c = null;

    @BeforeEach
    void apriori()
    {
        c = new Cliente("00001520","L", "DNI","07449492","SR.","ROLANDO","ROJAS LOBATON","988248223","rolando.rojas.lobaton@gmail.com" );
       // c.setCampanas(null);

    }

    @Test
    void testClientenumero()
    {
        assertTrue(c.getClientenumero().length() > 0,"Valor mayor a cero" );
        assertNotEquals("",c.getClientenumero());
    }

    @Test
    void testClientenumeroNotNull()
    {
        assertNotNull(c.getClientenumero());
    }

    @Test
    void testCampanaNotNull()
    {
        assertNotNull(c.getCampanas());
    }


}
